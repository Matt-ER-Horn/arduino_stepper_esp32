#include <Arduino.h>
#include <TMCStepper.h>
#include <AccelStepper.h>


/*
Stepper control library for Hein group ported to ESP32 with TMCStepper library w/ sensorless homing and accelstepper. Original library for arduino uno
and manual control of step and acceleration found at: https://gitlab.com/heingroup/arduino_stepper_c/-/tree/master/stepper_controller_interrupts_accel/src

Notes: in the old firmware to the CCR value refers to the value of the compare registers (OCR1A/B), these ultimately set the timing of the step 
pin. In the new code CCR is the step frequency (Steps/s). 


  Serial commands: original code / new code
  SCR: Set CCR (OCR1A value) / set maxiumum speed in steps/second
  SPS: Set position to go to in steps / set position to go to in steps
  SIR: Set initial comp register value for acceleration (each loop the stepper frequency increases) / set acceleration ( step/s^2) -- may need to adjust this parameter for some old hardware to work
  SHR: Set the home CCR value / set maximum speed in steps/second for homing
  SHD: Sets the home direction / Inverts all direction from current setting
  QCP: Query for current position relative to target position / query for steps to go
  QTP: Query for target position / query for target position
  QCR: Query for the CCR value / query for maximum speed
  QIR: Query for initial comp register value for acceleration / query for the acceleration value(steps/s^2)
  STP: Stops the motor / stops the motor with no deceleration
  HME: Rotates stepper motor until limit switch is pressed / rotates the motor until the sensorless homing stall value is reached and pin 18 is set to high
  BSY: Checks if the motor is busy / checks to see if the motor is running
  RLY: Sets the relay cut the power when not stepping / disable the stepper motor using the enable pin - shouldn't be necessary anymore since the stepper has auto low power when using the TMC 22099, idle current set in the setup() section below
  
  New Serial Commands:
  SMC: Set Motor Current - Ranges between 300 and 1800 mA 
  SMS: Set MicroSteps - set the number of microsteps (2,4,8,16,32,64,128,256)
  SSV: Set Stall Value  - Sets the stall value to use during homing
  ISV: Increase Stall Value by 5
  DSV: Decrease Stall Value by 5


  Responses
  -1: good
  -2: error in command
  -3: busy
  -4: done action/ not busy
*/


//Sensorless Homing Pin
#define STALL_PIN         18
//Parameters for the TMCStepper library-----------------------------------------------
#define EN_PIN           33 // Enable
#define DIR_PIN          26 // Direction
#define STEP_PIN         25 // Step
#define SERIAL_PORT Serial2 // TMC2208/TMC2224 HardwareSerial port
#define DRIVER_ADDRESS 0b00 // TMC2209 Driver address according to MS1 and MS2

#define R_SENSE 0.11f // Match to your driver

TMC2209Stepper driver(&SERIAL_PORT, R_SENSE, DRIVER_ADDRESS);  // Hardware Serial
//-------------------------------------------------------------------------------------


//accelstepper setup
AccelStepper stepper = AccelStepper(stepper.DRIVER, STEP_PIN, DIR_PIN);

int maxSpeed = 1000;  // speed in steps/second, replaces CCR
int homingSpeed = 500; // speed during homing in steps/second, replaces homingCCR
int acceleration = 1000; // acceleration value, functions differently that previous code which set an initial step rate and incremented it every loop
bool stepperEnable = true;
bool currentlyHoming = false;
int STALL_VALUE = 100;
bool shaft = false;


// Serial Communication

char buff[32];
char command[4];
volatile long cmd_value = 0;
#define CMD_LEN 3



//Functions---------------------------------------------------
void homing(){
  currentlyHoming = true; // set homing to true so nothing else can happen during homing

  stepper.setMaxSpeed(homingSpeed);
  stepper.setCurrentPosition(0);
  stepper.move(200); // move away from home position to make sure there is space to home
  while(stepper.distanceToGo() != 0){
    stepper.run();
  }
  digitalWrite(EN_PIN, HIGH);//make sure error condition is reset 
  delay(500);
  digitalWrite(EN_PIN, LOW);
  delay(500);
  //stepper.setSpeed(homingSpeed);
  stepper.move(-400000);
  while (stepper.currentPosition() > 100){ //run 200 steps to get stepper up to speed before the sensorless homing is activated
  stepper.run();
  //Serial.println(digitalRead(STALL_PIN)); //debug SG_Result
  }
  while(digitalRead(STALL_PIN) == LOW) { //activate sensorless homing - TODO do this as an interupt 
    stepper.run();
    // static uint32_t last_time=0;
    // uint32_t ms = millis();
    // stepper.run();
    // if((ms-last_time) > 100) { //run every 0.1s
    // last_time = ms;
    // Serial.println(driver.SG_RESULT());
    // }

  }
  
 stepper.setCurrentPosition(0);
 Serial.println("Done Homing");
 stepper.setMaxSpeed(maxSpeed);
 currentlyHoming = false;

  
}



void setup() {

//begin serial and stepper driver serial-----------------------------------
  Serial.begin(115200);
  Serial2.begin(115200);      //  UART Communcation with stepper driver
  
//Stepper driver setup------------------------------------
  driver.begin();
  driver.toff(1);  //4   // Sets the slow decay time (off time) [1... 15]. This setting also limits the maximum chopper frequency. For operation with StealthChop, this parameter is not used, but it is required to enable the motor. In case of operation with StealthChop only, any setting is OK.
  //driver.VACTUAL(speed); // VACTUAL allows moving the motor by UART control. It gives the motor velocity in +-(2^23)-1 [μsteps / t] 0: Normal operation. Driver reacts to STEP input./=0: Motor moves with the velocity given by VACTUAL. Step pulses can be monitored via INDEX output. The motor direction is controlled by the sign of VACTUAL.
  driver.blank_time(24); // Comparator blank time. This time needs to safely cover the switching event and the duration of the ringing on the sense resistor. For most applications, a setting of 16 or 24 is good. For highly capacitive loads, a setting of 32 or 40 will be required.
  driver.rms_current(900); // mA
  driver.microsteps(0);
  driver.TCOOLTHRS(0xFFFFF); // 20bit max // Lower threshold velocity for switching on smart energy CoolStep and StallGuard to DIAG output
  driver.semin(5);// CoolStep lower threshold [0... 15]. If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.  0: disable CoolStep
  driver.semax(2);// CoolStep upper threshold [0... 15].  If SG is sampled equal to or above this threshold enough times, CoolStep decreases the current to both coils.
  driver.sedn(0b01);  // Sets the number of StallGuard2 readings above the upper threshold necessary for each current decrement of the motor current.
  driver.SGTHRS(STALL_VALUE);  // StallGuard4 threshold [0... 255] level for stall detection. It compensates for motor specific characteristics and controls sensitivity. A higher value gives a higher sensitivity. A higher value makes StallGuard4 more sensitive and requires less torque to indicate a stall. The double of this value is compared to SG_RESULT. The stall output becomes active if SG_RESULT fall below this value.


//Accelstepper setup------------------------------------
  stepper.setMaxSpeed(maxSpeed); // Steps/s
  stepper.setAcceleration(acceleration); // step/s^2
  stepper.setPinsInverted(false, false, true); //Dir invert, Step invert, Enable invert
  stepper.enableOutputs();





//Set pins and Enable Stepper Driver------------------------------
  pinMode(EN_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  digitalWrite(EN_PIN, LOW); 

//setup stall pin to read config pin for sensorless homing----------
  pinMode(STALL_PIN, INPUT);


}


void loop()
{
  if(Serial.available()>0){
    uint8_t num_chars = Serial.readBytesUntil(
      0x0D, // Reads until it reaches terminal carriage return
      buff,
      sizeof(buff)
    );
    buff[num_chars] = '\0';
    sscanf(
      buff,
      "%3s %li",
      command,
      &cmd_value
    );
    command[3] = '\0';

    if (strncmp(command, "SCR", CMD_LEN)==0){ // Set max speed in Steps/Sec
      maxSpeed = cmd_value;
      stepper.setMaxSpeed(maxSpeed);
      Serial.println(maxSpeed);
    }
    else if (strncmp(command, "SPS", CMD_LEN)==0){ // Set new target in steps
      if (stepper.distanceToGo() != 0 || currentlyHoming == true){
        Serial.println("-3");
         }
      else{
          stepper.move(cmd_value);
          }
    }
    else if(strncmp(command, "SIR", CMD_LEN)==0){ // Set acceleration rate (In arudino uno version this set initial comp register value for acceleration)
      acceleration = cmd_value;
      stepper.setAcceleration(acceleration);
      Serial.println(cmd_value);
    }
    else if(strncmp(command, "SHR", CMD_LEN)==0){ // Set homing max speed
      homingSpeed = cmd_value;
      Serial.println(cmd_value);
    }
    else if(strncmp(command, "SHD", CMD_LEN)==0){ // reverses the homing direction
       shaft = !shaft;
       driver.shaft(shaft);
       Serial.println(cmd_value);
    }
    else if(strncmp(command, "QCP", CMD_LEN)==0){ // Query for current position relative to target position
      Serial.println(stepper.distanceToGo());
    }
    else if(strncmp(command, "QTP", CMD_LEN)==0){ // Query for target position
      Serial.println(stepper.targetPosition());
    }
    else if(strncmp(command, "QCR", CMD_LEN)==0){ // Query for max speed - in arudino uno version it is query for for the CCR value
      Serial.println(maxSpeed);
    }
    else if(strncmp(command, "QIR", CMD_LEN)==0){ // Query for accelleration value - in arduino uno version this is initial comp register value for acceleration
      Serial.println(acceleration);
    }
    else if(strncmp(command, "STP", CMD_LEN)==0){ // Stops the motor with decleration
      stepper.stop();
    }
    else if(strncmp(command, "SMC", CMD_LEN)==0){ // Stops the motor with decleration
      if (cmd_value < 1600){
        driver.rms_current(cmd_value);
        Serial.print("motor current set to ");
        Serial.println(cmd_value);
      } 
      else{
      Serial.println("-2");
      }
    }
    else if(strncmp(command, "RLY", CMD_LEN)==0){ // enable or disable the stepper
      if (cmd_value==0)
      {
        stepperEnable = false;
        stepper.disableOutputs();
      }
      else
      {
        stepperEnable = true;
        stepper.enableOutputs();
      }
      Serial.println(cmd_value);
    }
    else if(strncmp(command, "HME", CMD_LEN)==0){ // Rotates stepper motor until limit switch is pressed
      if (stepper.distanceToGo() != 0){
        Serial.println("-3");
      }
      else{
        Serial.println("-1");
        homing();
      }
    }
    else if(strncmp(command, "BSY", CMD_LEN)==0){ // Checks if the motor is busy
      if (stepper.distanceToGo() != 0 || currentlyHoming == true){
        Serial.println("-3");
      }
      else{
        Serial.println("-4");
      }
    }
    else{
      Serial.println("-2");
    }
  }
  stepper.run();
}